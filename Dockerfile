FROM node:10-alpine
WORKDIR /usr/src/app
COPY package*.json ./
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
RUN npm install pm2 -g
RUN npm install

COPY . .
EXPOSE 8001

